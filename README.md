# FINAL PROJECT
## KELOMPOK 5
-   Nursyafriady
-   Budi Haryanto
-   Sheva Ardhana

## TEMA
Protototype Website Kelurahan

## LINK VIDEO DEMO
- Gitlab : https://gitlab.com/ariboy972/final_project_laravel_record
- Google Drive : https://drive.google.com/file/d/1C4CdCIRrrZu435hom4wok4bRKylzPsCb/view?usp=sharing

## LINK DEPLOY
Nursyafriady : https://webkelurahan.nursyafriady.site/

## ERD
Terlampir di folder public/frontend/img/ERD_KelurahanSanbercode.png

## UI/UX TEMPLATE
Bootstrap, CSS, adobeXD, Photoshop

## LIBRARY/PACKAGE
Sweetalert, TinyMCE, data Tables, xZoom Container, Carbon

## SUMBER REFERENSI TEMPLATE/CONTENT
google (hanya untuk pembelajaran/prototype)

## PROSES/ALUR
Proses CRUD dan relasi antar table lebih ini diutamakan di bagian ADMIN/Backend antara lain : 
- berita
- gambar 
- data penduduk
- profile/pegawai
- gambar struktur organisasi
- kategori berita.

## RELATIONSHIP
- one to one : table user dan profile
- one to many : 
    - table user dan berita
    - table berita dengan gambar berita (galleri menggunakan library xZoom)
- many to many : 
    table berita dengan kategori/tags. yg baru bisa berjalan adalah relasi table kategori dengan berita (satu kategori bisa dimiliki oleh banyak berita)
- table struktur organisasi dan table penduduk : tidak ada relasi

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as: